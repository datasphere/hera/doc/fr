Un **attribut** est l'utilisation par une :ref:`entité <entité>` d'une :ref:`propriété <propriété>` particulière.

Outre l'entité et la propriété qu'il relie, chaque attribut est défini par une **cardinalité** qui indique son caractère **répétable** (ou non) ou **obligatoire** (ou non).

Un attribut peut aussi spécialiser (*ie.* surcharger) le libellé, la description et le type
de la :ref:`propriété` à laquelle il fait référence.
Cela n'altère en rien ladite propriété.
Cela permet d'en préciser l'usage ou en faciliter la compréhension dans le contexte posé par l'entité.
