Une **entité** est un sujet, un concept en rapport avec la thématique de la :ref:`base de données <base de données>` à laquelle elle appartient.
Une entité est caractérisée par des :ref:`attributs <attribut>` permettant de définir toutes les informations nécessaires à sa description.
