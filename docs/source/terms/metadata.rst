Une **métadonnée** est un élément atomique d'information.
Elle n'a de sens que si elle est associée à d'autres métadonnées ; l'agrégat ainsi formé est un :ref:`élément <élément>`.
Elle est obligatoirement relative à un :ref:`attribut <attribut>` dont elle représente la valeur pour cet élément (ou *une des* valeurs, si l'attribut est répétable).
