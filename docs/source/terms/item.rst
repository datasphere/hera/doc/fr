Un **élément** est un agrégat / composite d'information.
Chaque élément est la manifestation concrète d'une :ref:`entité <entité>`.
En conséquence, chacun de ses :ref:`attributs <attribut>` possède une valeur.
Chacune de ces valeurs constitue une information « atomique » à propos de l'élément, appelée :ref:`métadonnée <métadonnée>`.
