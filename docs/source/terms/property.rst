Une **propriété** est une caractéristique réutilisable susceptible d'être déclarée par une ou plusieurs :ref:`entités <entité>` d'une :ref:`base de données <base de données>`.

Chaque propriété est définie par :

* un **libellé** (nom) qui permet de l'identifier ;
* une **description** qui permet de comprendre comment l'utiliser ;
* un **type** qui contraint la valeur qu'il peut prendre ;
* une **URI** permettant de l'identifier de manière unique.
