L'ensemble des :ref:`entités <entité>` d'une base de données, ainsi que des :ref:`attributs <attribut>` de ces entités, est appelé le **schéma** de cette base de données.
