Une **base de données** est un ensemble d’informations *structurées* pour être facilement accessibles, organisées et mises à jour.
C’est une manière de stocker, mais surtout d’analyser de l’information sur une thématique donnée.
