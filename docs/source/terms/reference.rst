Une **référence** est un lien unique, direct, vers un autre :ref:`élément <élément>`.
Il s'agit d'un type spécifique de :ref:`métadonnée <métadonnée>`.
