:orphan:
.. index::
   single: termes; équivalences
.. _rosetta:

Équivalence entre les termes
----------------------------

Cette section complémente le :ref:`glossaire <glossaire>` et le :ref:`lexique <genindex>`.
Elle vise à rendre explicite:

#. (1\ :sup:`ère` colonne: **Terme français**) chaque terme utilisé par cette documentation en français ;
#. (2\ :sup:`ème` colonne: **Synonymes français**) le cas échéant, certains termes équivalents en français, parfois utilisés dans d'autres publications traitant des bases de données ;
#. (3\ :sup:`ème` colonne: **Terme anglais**) son équivalent utilisé par cette documentation en anglais ;
#. (4\ :sup:`ème` colonne: **Synonymes anglais**) le cas échéant, certains termes équivalents en anglais, parfois utilisés dans d'autres publications traitant des bases de données.

Pour les termes et leurs synonymes dans d'autres langues que le français et l'anglais, merci de vous référer à cette documentation dans la langue concernée, si elle existe.
Si la traduction que vous recherchez n'existe pas, n'hésitez pas à :ref:`proposer la vôtre <contribuer>` !

.. list-table:: Équivalence entre les termes
   :widths: 15 35 15 35
   :header-rows: 1

   * - Terme français
     - Synonymes français
     - Terme anglais
     - Synonymes anglais
   * - :term:`attribut`
     - champ, type
     - attribute
     - field
   * - :term:`base de données`
     -
     - database
     -
   * - :term:`élément`
     - | donnée, enregistrement,
       | objet, ligne (d'une table)
     - item
     - | data, entry, record,
       | object, (table) row
   * - :term:`entité`
     - concept, table, classe
     - entity
     - data type, record type, table, class
   * - :term:`métadonnée`
     - valeur, cellule
     - metadata
     - cell, value
   * - :term:`propriété`
     - champ, type
     - property
     - type, (base) field
   * - :term:`référence`
     - | relation, pointeur,
       | clé étrangère
     - reference
     - | pointer, reference,
       | foreign key, relationship
