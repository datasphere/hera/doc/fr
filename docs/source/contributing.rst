.. index::
   single: contribuer
   single: aide
.. _contribuer:

contribuer
==========

Vous songez à contribuer à cette documentation ?
Tout d'abord : merci !

- Vous avez trouvé une coquille que vous aimeriez corriger ? ...
- Vous pensez qu'un élément est mal expliqué, et souhaiteriez le clarifier ? ...
- Vous constatez un manque dans la documentation que vous sauriez combler ? ...
- Vous détectez une information obsolète que vous pensez pouvoir actualiser ? ...
- Vous désirez traduire cette documentation dans une autre langue ? ...

... Votre aide est la bienvenue !

| Ce projet utilise un système de tickets pour discuter des objectifs et tracer les tâches restantes ou déjà réalisées.
 En conséquence, n'hésitez pas à vous servir de ces tickets.
 Vous pourrez ainsi apporter votre contribution à une discussion existante.
 Vous pourrez aussi soumettre une demande ou mieux, proposer votre aide.
| Le gestionnaire de tickets se trouve à l'adresse suivante :
| |issues|



Architecture technique
----------------------

Chaque page de cette documentation est initialement écrite dans un fichier texte.
La syntaxe utilisée est reStructuredText [#rst-primer]_ [#rst-quick]_.
Il s'agit d'une syntaxe assez simple, propre à représenter des éléments complexes dans un simple fichier texte.
À titre d'exemple, vous pouvez cliquer sur le lien « Afficher la source de la page » qui se trouve peut-être en haut à droite de chaque page du site web : cela vous permettra de voir comment cette page est écrite en reStructuredText.

À chaque modification d'un de ces fichiers, le générateur de documentation Sphinx [#sphinx]_ produit différents formats de sortie, parmi lesquels un livre aux formats PDF ou EPUB, ainsi qu'un site web permettant de :ref:`retrouver facilement les informations <how_to_use_this>`.

Le site web généré est à son tour hébergé sur *Read the Docs* [#readthedocs]_, service reconnu d'hébergement de documentation.

Tout ceci est flexible, basé sur des logiciels libres, et parfaitement reproductible.

.. note::
   **Pourquoi ne pas avoir utilisé Markdown ?**
   
   Le principe de reStructuredText est similaire au Markdown/CommonMark [#md-commonmark]_.
   Cependant, ce dernier ne supporte pas par défaut certains éléments, tels que les tableaux, les encarts ou les notes en bas de page.
   De plus, Markdown n'est pas adapté à la génération automatique d'éléments tels qu'une table des matières ou d'un :ref:`index <genindex>`.
   Enfin, un des enjeux de ce document est la pérennité.
   Or, Markdown souffre de défauts flagrants au niveau conceptuel [#md-whats-wrong]_ [#md-mistakes]_, qui semblent être un frein à cet enjeu.

.. note::
   **Pourquoi ne pas avoir utilisé pandoc ?** [#pandoc]_
   
   Je n'ai rien contre pandoc : à un moment il a fallu choisir, c'est tout.
   Vous pouvez aussi générer cette documentation avec pandoc, si ça vous chante.
   Et si vous réalisez cela, pourquoi ne pas le partager avec la communauté ?

.. note::
   **Peut-on faire confiance à ReadTheDocs ?** [#readthedocs]_
   
   ReadTheDocs Community présente son offre tarifaire comme étant « gratuite à vie » pour les projets libres comme |project|.
   Évidemment, les promesses n'engagent que ceux qui les reçoivent.
   Cependant, cette offre court depuis 2010, ce qui est certainement à porter à leur crédit.

   | *Et si un jour ReadTheDocs suspend son offre ?*
   | Comme expliqué plus haut, la génération de cette documentation est entièrement reproductible.
     Il est donc aisé de l'héberger ailleurs, par exemple dans un entrepôt institutionnel pérenne.
     Il est aussi envisageable de ne l'héberger nulle part, puisque chacun peut librement la reconstruire chez soi.
   | Dans tous les cas, cela ne rendra pas cette documentation indisponible,
     puisqu'un site miroir existe d'ores et déjà à l'adresse suivante :
     |mirror|

   | *Et si un jour ReadTheDocs « dérobe » des données ?*
   | |project| est un logiciel libre, et sa documentation ne fait pas exception.
     C'est un don gratuit et sans limite de temps, à toute personne qui souhaite en profiter.
     Si cela vous apporte quelque frisson, vous pouvez imaginer que vous le « volez ».
     Houlala ! (づ｡◕‿‿◕｡)づ



.. rubric:: Références

.. [#rst-primer] `reStructuredText Primer <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_
.. [#rst-quick] `Quick reStructuredText <https://docutils.sourceforge.io/docs/user/rst/quickref.html>`_
.. [#sphinx] `Site officiel du logiciel Sphinx <https://www.sphinx-doc.org/>`_
.. [#readthedocs] `Site officiel Read the Docs <https://readthedocs.org/>`_
.. [#md-commonmark] `L'initiative CommonMark <https://commonmark.org/>`_, une des (trop) nombreuses spécifications de Markdown.
.. [#md-whats-wrong] `What's Wrong with Markdown? <https://www.adamhyde.net/whats-wrong-with-markdown/>`_, par Adam Hyde.
.. [#md-mistakes] `6 Things Markdown Got Wrong <https://www.swyx.io/markdown-mistakes>`_ (swyx)
.. [#pandoc] `Site officiel du logiciel pandoc <https://pandoc.org/>`_
