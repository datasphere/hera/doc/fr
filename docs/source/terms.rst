.. index::
   single: glossaire
   single: termes
.. _glossaire:

=========
Glossaire
=========

Vous pouvez retrouver tous les termes ci-dessous :ref:`dans l'index <genindex>`.
Certains possèdent parfois :ref:`un ou plusieurs équivalents <rosetta>`.



.. glossary::
  :sorted:

  base de données
    .. include:: /terms/database.rst
    :ref:`En savoir plus <base de données>`

  schéma
    .. include:: /terms/schema.rst
    :ref:`En savoir plus <schema>`

  propriété
    .. include:: /terms/property.rst
    :ref:`En savoir plus <propriété>`

  entité
    .. include:: /terms/entity.rst
    :ref:`En savoir plus <entité>`

  attribut
    .. include:: /terms/attribute.rst
    :ref:`En savoir plus <attribut>`

  élément
    .. include:: /terms/item.rst
    :ref:`En savoir plus <élément>`

  métadonnée
    .. include:: /terms/metadata.rst
    :ref:`En savoir plus <métadonnée>`

  référence
    .. include:: /terms/reference.rst
    :ref:`En savoir plus <référence>`

  relation
    .. include:: /terms/relationship.rst
    :ref:`En savoir plus <relation>`
