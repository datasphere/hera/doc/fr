.. index::
   single: concepts
   single: vocabulaire
.. |date| date::

concepts
========

Ce chapitre décrit les principaux concepts utilisés par |project|, et le vocabulaire utilisé.
Lorsque cela est possible, des termes alternatifs sont donnés.
Ceux-ci sont donnés pour faciliter la compréhension des personnes ayant déjà une expérience dans les bases de données.
Ces termes alternatifs, et leurs équivalents anglais sont :ref:`rappelés ici <rosetta>`.

Ce chapitre s'adressant à des lecteurs et lectrices aux expériences et bagages techniques différents,
il tente d'expliquer chaque concept de (jusqu'à) 3 manières différentes :
une description en français, un exemple des structures correspondant
`en JSON <https://fr.wikipedia.org/wiki/JavaScript_Object_Notation>`_, et
un `diagramme de classes UML <https://fr.wikipedia.org/wiki/Diagramme_de_classes>`_.

.. note::

   Étant donné que ces 3 manières d'expliquer sont équivalentes, vous
   êtes encouragés à y piocher uniquement ce qui vous « parle » le plus.
   Par exemple, si vous ne parlez pas
   le `langage UML <https://fr.wikipedia.org/wiki/UML_(informatique)>`_,
   n'hésitez pas à ignorer tous ces diagrammes bizarres !



.. contents:: Sommaire
   :local:





.. index::
   double: concepts; base de données
   single: base de données
.. _base de données:

base de données
---------------

.. include:: /terms/database.rst

.. index::
   single: schéma
.. _schema:

La vocation d'une base de données n'est pas de modéliser la totalité de la réalité, mais uniquement les concepts, ou :ref:`entités <entité>`, nécessaires pour représenter une thématique donnée.
En cela, et tout particulièrement dans un contexte scientifique, une base de données représente aussi une *vision*, une certaine manière d'aborder une problématique de recherche, intrinsèquement liée aux membres de l'équipe qui participent à sa construction.

.. include:: /terms/schema.rst
Ce schéma représente est la modélisation complète de la thématique concernée, parfois la réponse à la problématique.
C'est la vision de la personne ou de l'équipe conceptrice de la base de données.

Pour pouvoir gérer toutes les bases de données imaginables, |project| les abstrait en 5 concepts principaux : :ref:`entité`, :ref:`propriété`, :ref:`attribut`, :ref:`élément`, :ref:`métadonnée`.


.. plantuml::
   :caption: HERA: 5 concepts abstraits :
      :ref:`propriété`, :ref:`entité`, :ref:`attribut`,
      :ref:`élément`, :ref:`métadonnée`
   :align: center

   @startmindmap
   * Base de données
   --[#Orange] Propriétés
   --[#Sienna] Entités
   --[#FireBrick] Attributs
   ++[#MediumPurple] Éléments
   ++[#RoyalBlue] Métadonnées
   @endmindmap


.. plantuml:: ../../resource/figure/hera.uml
   :caption: (UML) HERA: `Diagramme de classes <https://fr.wikipedia.org/wiki/Diagramme_de_classes>`_ :
      :ref:`Propriété <propriété>` (*Property*),
      :ref:`Entité <entité>` (*Entity*),
      :ref:`Attribut <attribut>` (*Attribute*),
      :ref:`Élément <élément>` (*Item*),
      :ref:`Métadonnée <métadonnée>` (*Metadata*)
   :align: center




.. index::
   double: concepts; propriété
   single: propriété
   single: type
.. _propriété:

propriété
---------

.. include:: /terms/property.rst

.. plantuml::
   :caption: (modèle JSON) Une propriété
   :width: 60%
   :align: center

   @startjson
   !include ../../resource/figure/hera.property.json
   @endjson


.. plantuml::
   :caption: (UML) `Diagramme de classes <https://fr.wikipedia.org/wiki/Diagramme_de_classes>`_ :
      :ref:`Propriété <propriété>` (*Property*)
   :align: center

   @startuml
   !include ../../resource/figure/hera.uml
   remove Entity
   remove Attribute
   remove Item
   remove Metadata
   @enduml








.. index::
   double: concepts; entité
   single: entité
   single: table
   single: classe
.. _entité:

entité
------

.. include:: /terms/entity.rst

La liste des attributs d'une entité n'a pas forcément vocation à être exhaustive.
Il s'agit simplement de modéliser ce dont on a besoin dans le cadre d'une thématique précise.


.. plantuml::
   :caption: (modèle JSON) Une entité liste ses :ref:`attributs <attribut>`,
      dont chacun est l'utilisation concrète d'une :ref:`propriété`
      plus abstraite par cette entité
   :width: 100%
   :align: center

   @startjson
   !include ../../resource/figure/hera.entity.json
   @endjson


.. plantuml::
   :caption: (UML) `Diagramme de classes <https://fr.wikipedia.org/wiki/Diagramme_de_classes>`_ :
      :ref:`Propriété <propriété>` (*Property*),
      :ref:`Entité <entité>` (*Entity*),
      :ref:`Attribut <attribut>` (*Attribute*)
   :align: center

   @startuml
   !include ../../resource/figure/hera.uml
   remove Item
   remove Metadata
   @enduml





.. index::
   double: concepts; attribut
   single: attribut
   single: table; colonne
   single: champ
.. _attribut:

attribut
--------

.. include:: /terms/attribute.rst

.. plantuml::
   :caption: (UML) `Diagramme de classes <https://fr.wikipedia.org/wiki/Diagramme_de_classes>`_ :
      :ref:`Attribut <attribut>` (*Attribute*)
   :align: center

   @startuml
   !include ../../resource/figure/hera.uml
   remove Property
   remove Entity
   remove Item
   remove Metadata
   @enduml

.. note::

   En pratique, la `cardinalité <https://fr.wikipedia.org/wiki/Cardinalit%C3%A9_(programmation)>`_
   d'un attribut est représentée par un nombre minimal (``min``) et maximal (``max``) d'occurences
   de la métadonnée concernée pour chaque :ref:`élément` de l':ref:`entité` à laquelle l'attribut appartient.
   Par exemple:

   - si la métadonnée est obligatoire pour chaque élément de cette entité, ``min`` vaut 1
   - si la métadonnée est au contraire optionnelle, ``min`` vaut 0
   - si la métadonnée n'est pas répétable, ``max`` vaut 1
   - si la métadonnée est répétable, ``max`` vaut ... autant que vous voulez !





.. index::
   double: concepts; élément
   single: élément
   single: objet
   single: enregistrement
   single: table; ligne
.. _élément:

élément
-------

.. include:: /terms/item.rst


.. plantuml::
   :caption: (modèle JSON) Un Élément est simplement la liste de
      ses :ref:`métadonnées <métadonnée>` ; chaque métadonnée est la valeur
      concrète d'une :ref:`propriété`.
   :width: 90%
   :align: center

   @startjson
   !include ../../resource/figure/hera.item.json
   @endjson


.. plantuml::
   :caption: (UML) `Diagramme de classes <https://fr.wikipedia.org/wiki/Diagramme_de_classes>`_ :
      :ref:`Élément <élément>` (*Item*), :ref:`Métadonnée <métadonnée>` (*Metadata*)
   :align: center

   @startuml
   !include ../../resource/figure/hera.uml
   remove Entity
   remove Property
   remove Attribute
   @enduml





.. index::
   single: concepts; métadonnée
   single: métadonnée
.. _métadonnée:

métadonnée
----------

.. include:: /terms/metadata.rst





.. index::
   double: concepts; référence
   single: référence
   single: pointeur
.. _référence:

référence
---------

.. include:: /terms/reference.rst





.. index::
   double: concepts; relation
   single: relation
.. _relation:

relation
--------

.. include:: /terms/relationship.rst
